This is a Kotlin Multiplatform project targeting Android, iOS, Desktop

In this basic project we have created cross platform application that would run on Android and IOS Devices and along with that it will also on desktop


![Screenshot 2024-06-19 at 9.18.30 PM.png](Screenshot%202024-06-19%20at%209.18.30%E2%80%AFPM.png)

![Simulator Screenshot - iPhone 15 - 2024-06-19 at 21.08.27.png](Simulator%20Screenshot%20-%20iPhone%2015%20-%202024-06-19%20at%2021.08.27.png)

![Welcome Page.png](Welcome%20Page.png)